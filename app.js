/**
 * Created by saumilpatel on 2014/03/24.
 */
(function(){
    var canvas = document.getElementsByTagName('canvas')[0],
        ctx = canvas.getContext('2d'),
        radius = 10,
        info = document.getElementById('info');

    //set the canvas cover as the screen
    canvas.width = document.body.clientWidth;
    canvas.height = document.body.clientHeight;

    // move the context co-ordinates to the bottom middle of the screen
    ctx.translate(canvas.width/2, canvas.height);

    ctx.fillStyle = "rgba(0,0,0,0.9)";
    ctx.strokeStyle = "rgba(255,0,0,0.9)";
    ctx.lineWidth = 5;

    function draw(frame){
        var pos, i , len, data = [];

        ctx.fillStyle = "rgba(255, 255, 255, 0.1)";
        ctx.fillRect(-canvas.width/2, -canvas.height, canvas.width, canvas.height);

        ctx.fillStyle = "rgba(0,0,0,0.9)";

        for(i = 0, len = frame.pointables.length ; i < len; i++){
            pos = frame.pointables[i].tipPosition;

            data.push(pos);

            ctx.beginPath();
            ctx.arc( pos.x - radius/2, -(pos.y - radius/2), radius, 0, 2*Math.PI );
            ctx.fill();
            ctx.stroke();
        }

        //To show points on the canvas
        //info.innerHTML = data.join(', ');
    };

    Leap.loop(draw);
})();